﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Player : MonoBehaviour {

  public float movementSpeed = 25.0f;
  public float jumpHeight = 250.0f;
  public float horizontalDrag = 2.0f;

  private Rigidbody rb;
  
	private void Start () {
    rb = GetComponent<Rigidbody>();
	}
	
	private void FixedUpdate () {
    if (isGrounded(rb, 0.1f)) {
      if (Input.GetKeyDown("space")) {
        rb = jump(rb, jumpHeight);
      }
    }

    if (isGrounded(rb, 1.0f)) {
      rb.velocity += walk(transform, movementSpeed, horizontalDrag);
    }
 	}

  private bool isGrounded(Rigidbody rb, float tolerance) {
    return (rb.velocity.y < tolerance) && (rb.velocity.y > -tolerance);
  }

  private Vector3 walk(Transform t, float speed, float drag) {
    float xin = Input.GetAxisRaw("Horizontal");
    float zin = Input.GetAxisRaw("Vertical");
    return (((zin * t.forward * (1.0f / drag)) + (xin * t.right * (1.0f / drag)))) * speed * Time.deltaTime;
  }

  private Rigidbody jump(Rigidbody rb, float height) {
    rb.AddRelativeForce(transform.up * jumpHeight);
    return rb;
  }
}
