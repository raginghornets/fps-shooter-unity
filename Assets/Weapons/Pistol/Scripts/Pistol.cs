﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pistol : MonoBehaviour {

  public GameObject projectileSpawn;
  public GameObject projectile;
  public float bulletSpeed = 100.0f;

  private AudioSource audioData;

  private void Start() {
    audioData = GetComponent<AudioSource>();
  }

  private void Update() {
    if (Input.GetButtonDown("Fire1")) {
      GameObject projectileClone = Shoot(projectile, projectileSpawn.transform, bulletSpeed);
      audioData.Play();
      Destroy(projectileClone, 5.0f);
    }
  }

  private GameObject Shoot(GameObject projectile, Transform t, float speed) {
    GameObject projectileClone = Instantiate(projectile, t.position, t.rotation);
    projectileClone.GetComponent<Rigidbody>().velocity = -t.up * speed;
    return projectileClone;
  }

}